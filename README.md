# Accounting App

The Accounting App is a full application to make bank transactions through a RESTful API and listing them on a web page.


## Description
* **API**: This folder contains the API that was built on [GO](https://golang.org/).
* **Client**: This folder contains the web page that is an [Angular](https://angular.io/) application.
* **Resouces** This folder contains a [postman](https://www.postman.com/) collection


## Approach
**GO**: The API approach consists of separate responsibilities by isolating packages (`handlers`, `services`, `errors`, `storage`, and `tests`) for every functionality, and each of the talks by itself. Moreover, the code quality is based (mostly) on GO conventions and best practices.

**Angular**: The angular project approach is quite simple and basic (since just use one static page, no further configurations, and project structure is needed). To run this app isn't necessary to install angular. There is a **/dist** directory versioned into **/client** (that has the static files built by angular), and able to be run by [lite-server](https://github.com/johnpapa/lite-server).

## Requirements

* **Golang** (if you are not able to run a .exe file)
* **lite-server** (the web server)
* **Postman** (api testing tool)

## Installation

Use a package manager (such as npm) to install  [lite-server](https://github.com/johnpapa/lite-server):

```bash
npm install lite-server
```

## Usage

1. Clone the repo and get in there.
1. Serve the API (you have two options for that. Choose the better fits to you)
	1. Executing the **main.exe** file under **/api** directory `./api/main.exe`
    1. Using the GO commands by navigating to **/api** directory and execute `go run main.go`
1. Serve the client using the following command line:

```bash
lite-server --baseDir="client/dist/client"
```


## Testing
To get started testing the GO API, import the Postman collection allowed into the **/resources** directory into the Postman client.

There are a couple of unit tests in the api that may interest to run. To run them, navigate to **/api/tests** directory and just hit the following command:

```go
go test -v
```

The above command executes all unit tests.