module bitbucket.org/morarick/accounting_app/api

go 1.13

require (
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/hashicorp/go-memdb v1.2.1
)
