package handlers

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/morarick/accounting_app/api/errors"
	"bitbucket.org/morarick/accounting_app/api/services"
)

//FetchAccountBalance handle the fetching for the current account balance
func FetchAccountBalance(responseWriter http.ResponseWriter, r *http.Request) error {

	//The ID is Hardcoded beacuse of there is just one account
	account := &services.Account{
		ID: "1",
	}

	if err := account.FindAccountByID(); err != nil {
		return errors.NewHTTPError(err, http.StatusInternalServerError, "The ID was invalid or could not be found")
	}

	accountJSON, err := json.Marshal(account.Value)
	if err != nil {
		return errors.NewHTTPError(err, http.StatusInternalServerError, "Error parsing response json")
	}

	responseWriter.Header().Add("Content-type", "application/json")
	responseWriter.Header().Add("Access-Control-Allow-Origin", "*")
	responseWriter.Write(accountJSON)

	return nil
}
