package handlers

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"bitbucket.org/morarick/accounting_app/api/errors"
)

//RootHandler function type to override the ServeHTTP method and handle errors there
type RootHandler func(http.ResponseWriter, *http.Request) error

func (fn RootHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := fn(w, r)
	if err == nil {
		return
	}

	log.Printf("An error accured: %v", err)

	clientError, ok := err.(errors.ClientError)
	if !ok {
		w.WriteHeader(500)
		return
	}

	body, err := clientError.ResponseBody()
	if err != nil {
		log.Printf("An error accured: %v", err)
		w.WriteHeader(500)
		return
	}

	status, headers := clientError.ResponseHeaders()
	for k, v := range headers {
		w.Header().Set(k, v)
	}
	w.WriteHeader(status)
	w.Write(body)
}

//InitHandlers initzialize all handlers by routing them
func InitHandlers() {
	router := mux.NewRouter()

	handleTransactionRoutes(router)
	handleAccountRoutes(router)
	println("Go API running at port 3000")
	_ = http.ListenAndServe(":3000", router)
}

func handleTransactionRoutes(router *mux.Router) {
	router.Handle("/api/transactions", RootHandler(AddTransaction)).Methods(http.MethodPost)
	router.Handle("/api/transactions", RootHandler(FetchTransactionHistory)).Methods(http.MethodGet)
	router.Handle("/api/transactions/{id}", RootHandler(FindTransactionByID)).Methods(http.MethodGet)
}

func handleAccountRoutes(router *mux.Router) {
	router.Handle("/api", RootHandler(FetchAccountBalance)).Methods(http.MethodGet)
}
