package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"

	"bitbucket.org/morarick/accounting_app/api/errors"
	"bitbucket.org/morarick/accounting_app/api/services"
)

//AddTransaction handle the adding for a new transaction
func AddTransaction(responseWriter http.ResponseWriter, request *http.Request) error {

	var transaction services.Transaction

	if err := json.NewDecoder(request.Body).Decode(&transaction); err != nil {
		return errors.NewHTTPError(err, http.StatusBadRequest, "Bad request : invalid JSON.")
	}

	id, err := uuid.NewUUID()
	if err != nil {
		return errors.NewHTTPError(err, http.StatusInternalServerError, "Error generating new UUID")
	}

	transaction.ID = id.String()
	transaction.EffectiveDate = time.Now().Format(time.RFC3339)

	if err = validateTransaction(&transaction); err != nil {
		return errors.NewHTTPError(err, http.StatusConflict, err.Error())
	}

	if err = transaction.Add(); err != nil {
		return errors.NewHTTPError(err, http.StatusConflict, "Wrong transaction")
	}

	if err = updateAccountBalanceByTransaction(transaction); err != nil {
		return errors.NewHTTPError(err, http.StatusConflict, "Error updating account balance after "+transaction.Type+" transaction finished")
	}

	if err := parseObjectToJSONAndWriteResponse(responseWriter, transaction); err != nil {
		return errors.NewHTTPError(err, http.StatusInternalServerError, "Error parsing response json")
	}

	return nil
}

func updateAccountBalanceByTransaction(transaction services.Transaction) error {

	//As there is just one user, the ID is Hardcoded
	account := &services.Account{
		ID: "1",
	}

	if err := account.FindAccountByID(); err != nil {
		return err
	}

	switch transaction.Type {
	case services.CreditTransactionType:
		account.Value += transaction.Amount
		break
	case services.DebitTransactionType:
		account.Value -= transaction.Amount
		break
	}

	if err := account.CreateOrUpdateAccount(); err != nil {
		return errors.NewHTTPError(err, http.StatusInternalServerError, "Error updating the Account Balance")
	}
	return nil
}

//FindTransactionByID handle the search of a transaction by a given id
func FindTransactionByID(responseWriter http.ResponseWriter, request *http.Request) error {

	transaction := &services.Transaction{
		ID: mux.Vars(request)["id"],
	}

	//Here we get a new transaction object by reference
	if err := transaction.FindByID(); err != nil {
		return errors.NewHTTPError(err, http.StatusConflict, "The ID was invalid or could not be found")
	}

	if err := parseObjectToJSONAndWriteResponse(responseWriter, transaction); err != nil {
		return errors.NewHTTPError(err, http.StatusInternalServerError, "Error parsing response json")
	}

	return nil
}

//FetchTransactionHistory dandle the fetching for the transaction history
func FetchTransactionHistory(responseWriter http.ResponseWriter, request *http.Request) error {

	transaction := &services.Transaction{}

	//Here we get a new transaction object by reference
	transactions, err := transaction.FindAll()
	if err != nil {
		return errors.NewHTTPError(err, http.StatusInternalServerError, "Error iterating history results")
	}

	if err := parseObjectToJSONAndWriteResponse(responseWriter, transactions); err != nil {
		return errors.NewHTTPError(err, http.StatusInternalServerError, "Error parsing response json")
	}

	return nil
}

func parseObjectToJSONAndWriteResponse(responseWriter http.ResponseWriter, object interface{}) error {

	jsonObject, err := json.Marshal(object)
	if err != nil {
		return err
	}

	responseWriter.Header().Add("Content-type", "application/json")
	responseWriter.Header().Add("Access-Control-Allow-Origin", "*")
	responseWriter.Write(jsonObject)

	return nil
}

func validateTransaction(transaction *services.Transaction) error {

	if transaction.Amount <= 0.0 {
		return fmt.Errorf("Invalid Amount. Negative amounts are not allowed for transactions")
	}

	switch transaction.Type {
	case services.CreditTransactionType:
		break
	case services.DebitTransactionType:
		break
	default:
		return fmt.Errorf("Invalid transaction type. Only 'debit' and 'credit' are valid transaction types")
	}

	return nil
}
