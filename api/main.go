package main

import (
	"bitbucket.org/morarick/accounting_app/api/handlers"
	"bitbucket.org/morarick/accounting_app/api/services"
	"bitbucket.org/morarick/accounting_app/api/storage"
)

func main() {
	storage.InitMemoryStorage()

	//It is necessary to insert an account where to carry out transactions
	account := &services.Account{
		ID:    "1",
		Value: 0.0,
	}
	account.CreateOrUpdateAccount()

	handlers.InitHandlers()
}
