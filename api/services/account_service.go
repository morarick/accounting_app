package services

import (
	"fmt"

	"bitbucket.org/morarick/accounting_app/api/storage"
)

//Account is a type that store the minimal fields for an account
type Account struct {
	ID    string  `json:"id"`
	Value float64 `json:"value"`
}

//FindAccountByID search an account by a given ID in the in-memmory storage
func (account *Account) FindAccountByID() error {

	txn := storage.MemoryDB.Txn(false)
	defer txn.Abort()

	accountInterface, err := txn.First("account", "id", account.ID)
	if err != nil || accountInterface == nil {
		return fmt.Errorf("The ID was invalid or could not be found: %v", account.ID)
	}

	*account = *accountInterface.(*Account)

	return nil
}

//CreateOrUpdateAccount create or update (if exists) a new account in the in-memmory storage
func (account *Account) CreateOrUpdateAccount() error {

	txn := storage.MemoryDB.Txn(true)
	if err := txn.Insert("account", account); err != nil {
		return err
	}

	txn.Commit()

	return nil
}
