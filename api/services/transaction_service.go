package services

import (
	"fmt"

	"bitbucket.org/morarick/accounting_app/api/storage"
)

//Transaction is a type that contains the minimal attributes for a bank transaction
type Transaction struct {
	ID            string  `json:"id"`
	Type          string  `json:"type"`
	Amount        float64 `json:"amount"`
	EffectiveDate string  `json:"effectiveDate"`
}

const (
	//CreditTransactionType is a constant for a credit transaction type
	CreditTransactionType string = "credit"

	//DebitTransactionType is a constant for a debit transaction type
	DebitTransactionType string = "debit"
)

//Add add a new transaction to the in-memory storage
func (transaction *Transaction) Add() error {

	txn := storage.MemoryDB.Txn(true)
	if err := txn.Insert("transaction", transaction); err != nil {
		return err
	}

	txn.Commit()

	return nil
}

//FindByID find a transaction by a given ID in the the in-memory storage
func (transaction *Transaction) FindByID() error {

	txn := storage.MemoryDB.Txn(false)
	defer txn.Abort()

	transactionInterface, err := txn.First("transaction", "id", transaction.ID)
	if err != nil || transactionInterface == nil {
		return fmt.Errorf("The ID was invalid or could not be found: %v", transaction.ID)
	}

	*transaction = *transactionInterface.(*Transaction)

	return nil
}

//FindAll find all transactions stored at memory
func (transaction *Transaction) FindAll() ([]Transaction, error) {

	txn := storage.MemoryDB.Txn(false)
	defer txn.Abort()

	transactions := []Transaction{}

	it, err := txn.Get("transaction", "id")
	if err != nil {
		return transactions, err
	}

	for obj := it.Next(); obj != nil; obj = it.Next() {
		transactions = append(transactions, *obj.(*Transaction))
	}

	return transactions, nil
}
