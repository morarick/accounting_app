package storage

import (
	"github.com/hashicorp/go-memdb"
)

//MemoryDB memory database object to be used by the services package
var MemoryDB *memdb.MemDB

//InitMemoryStorage initialize a new memory storage at runtime
func InitMemoryStorage() {
	schema, err := createSchema()
	if err != nil {
		panic(err)
	}

	MemoryDB, err = memdb.NewMemDB(schema)
	if err != nil {
		panic(err)
	}
}

func createSchema() (*memdb.DBSchema, error) {

	schema := &memdb.DBSchema{
		Tables: map[string]*memdb.TableSchema{
			"transaction": createTransactionTable(),
			"account":     createAccountTable(),
		},
	}
	err := schema.Validate()
	if err != nil {
		return nil, err
	}
	return schema, err
}

func createTransactionTable() *memdb.TableSchema {

	id := &memdb.IndexSchema{
		Name:    "id",
		Unique:  true,
		Indexer: &memdb.StringFieldIndex{Field: "ID"},
	}
	transactionTable := &memdb.TableSchema{
		Name: "transaction",
		Indexes: map[string]*memdb.IndexSchema{
			"id": id,
		},
	}
	return transactionTable
}

func createAccountTable() *memdb.TableSchema {

	id := &memdb.IndexSchema{
		Name:    "id",
		Unique:  true,
		Indexer: &memdb.StringFieldIndex{Field: "ID"},
	}
	accountTable := &memdb.TableSchema{
		Name: "account",
		Indexes: map[string]*memdb.IndexSchema{
			"id": id,
		},
	}
	return accountTable
}
