package tests

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/morarick/accounting_app/api/handlers"
	"bitbucket.org/morarick/accounting_app/api/services"
	"bitbucket.org/morarick/accounting_app/api/storage"
)

func TestAddTransaction_Success(t *testing.T) {
	storage.InitMemoryStorage()
	account := &services.Account{
		ID:    "1",
		Value: 0.0,
	}
	account.CreateOrUpdateAccount()

	var jsonStr = []byte(`{"type":"credit","amount":50}`)

	req, err := http.NewRequest("POST", "/transactions", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.Handler(handlers.RootHandler(handlers.AddTransaction))
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

func TestAddTransaction_Conflict_InvalidTransactionType(t *testing.T) {
	storage.InitMemoryStorage()
	account := &services.Account{
		ID:    "1",
		Value: 0.0,
	}
	account.CreateOrUpdateAccount()

	var jsonStr = []byte(`{"type":"invalid","amount":50}`)

	req, err := http.NewRequest("POST", "/transactions", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.Handler(handlers.RootHandler(handlers.AddTransaction))
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status == http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := `{"detail":"Invalid transaction type. Only 'debit' and 'credit' are valid transaction types"}`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestAddTransaction_Conflict_NegativeAmount(t *testing.T) {
	storage.InitMemoryStorage()
	account := &services.Account{
		ID:    "1",
		Value: 0.0,
	}
	account.CreateOrUpdateAccount()

	var jsonStr = []byte(`{"type":"credit","amount":-50}`)

	req, err := http.NewRequest("POST", "/transactions", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.Handler(handlers.RootHandler(handlers.AddTransaction))
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status == http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := `{"detail":"Invalid Amount. Negative amounts are not allowed for transactions"}`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestAddTransaction_BadRequest_InvalidTransactionType(t *testing.T) {
	storage.InitMemoryStorage()
	account := &services.Account{
		ID:    "1",
		Value: 0.0,
	}
	account.CreateOrUpdateAccount()

	var jsonStr = []byte(`{"type":10,"amount":50}`)

	req, err := http.NewRequest("POST", "/transactions", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.Handler(handlers.RootHandler(handlers.AddTransaction))
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status == http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := `{"detail":"Bad request : invalid JSON."}`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestAddTransaction_BadRequest_InvalidAmount(t *testing.T) {
	storage.InitMemoryStorage()
	account := &services.Account{
		ID:    "1",
		Value: 0.0,
	}
	account.CreateOrUpdateAccount()

	var jsonStr = []byte(`{"type":"credit","amount":"50}`)

	req, err := http.NewRequest("POST", "/transactions", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.Handler(handlers.RootHandler(handlers.AddTransaction))
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status == http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := `{"detail":"Bad request : invalid JSON."}`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}
