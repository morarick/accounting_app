package tests

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/morarick/accounting_app/api/handlers"
	"bitbucket.org/morarick/accounting_app/api/services"
	"bitbucket.org/morarick/accounting_app/api/storage"
)

func TestFetchTransactionHistory_Success(t *testing.T) {
	storage.InitMemoryStorage()

	transaction := &services.Transaction{
		ID:            "1",
		Amount:        50.0,
		EffectiveDate: "2020-05-03T22:34:38.573Z",
		Type:          services.CreditTransactionType,
	}
	transaction.Add()

	req, err := http.NewRequest("GET", "/transactions", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.Handler(handlers.RootHandler(handlers.FetchTransactionHistory))
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := `[{"id":"1","type":"credit","amount":50,"effectiveDate":"2020-05-03T22:34:38.573Z"}]`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}
