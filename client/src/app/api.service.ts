import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ApiService {

    constructor(private http: HttpClient) { }

    baseUrl: string = "http://localhost:3000/api";

    get(resource: string) : Observable<any> {
        return this.http.get(`${this.baseUrl}${resource}`);
    }
}
