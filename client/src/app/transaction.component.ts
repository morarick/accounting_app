import { Component, OnInit } from '@angular/core';
import { Transaction } from './transaction.model'
import { TransactionService } from './transaction.service'

import { Subject } from 'rxjs';
@Component({
  selector: 'app-root',
  templateUrl: './transaction.component.html',
  styles: []
})
export class TransactionComponent implements OnInit {
  title = 'Accounting App';
  transactions: Transaction[];
  currentAccountBalance: string;

  constructor(
    private transactionService: TransactionService,
  ) { }

  ngOnInit() {
    this.transactionService.getTransactionHistory().subscribe(transactions => {
      this.transactions = transactions;
    });

    this.transactionService.getCurrentAccountBalance().subscribe(currentAccountBalance => {
      this.currentAccountBalance = currentAccountBalance;
    });
  };
}
