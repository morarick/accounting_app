export class Transaction {

    constructor(options?: any) {
        if(!options) {
            return;
        }

        this.id = options.id;
        this.type = options.type;
        this.amount = options.amount;
        this.effectiveDate = options.effectiveDate;
    }

    id: string;
    type: string;
    amount: Float64Array;
    effectiveDate: string;
}