import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { TransactionComponent } from './transaction.component';
import { ApiService } from './api.service';
import { TransactionService } from './transaction.service';

@NgModule({
  declarations: [
    TransactionComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    ApiService,
    TransactionService,
  ],
  bootstrap: [TransactionComponent]
})
export class AppModule { }
