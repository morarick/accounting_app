import { Injectable } from '@angular/core';
import { Transaction } from './transaction.model';
import * as _ from 'lodash';

import { Observable, } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiService } from './api.service';

@Injectable()
export class TransactionService {

    constructor(
        private apiService: ApiService,
    ) {}

    getTransactionHistory(): Observable<Transaction[]> {
        return this.apiService.get(`/transactions`).pipe(map(data => 
            _.map(data, item => new Transaction(item))
        ));
    }

    getCurrentAccountBalance(): Observable<string> {
        return this.apiService.get(``);
    }
}